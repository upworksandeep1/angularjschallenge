describe('applications_controller', function() {
    var scope, $state, $http, ngDialog, applicationsController;
    //Mock data
    var group = JSON.Stringify({pets :  1 , expanded : true, rental_percent_of_income :40 });
    var dummyCSSClass ;


    beforeEach(inject(function ($rootScope, $controller ,$state, $http, ngDialog) {
        
        scope = $rootScope.$new();

        applicationsController = function() {
            return $controller('applications_controller', {
                '$scope': scope
            });
        };
    }));

    it('should have a method to check if user has Pets', function() {
        var controller = applicationsController();
        var result = controller.hasPets(group);
        expect(result).Not.toBeNull();
       
    });
    it('should check if in group any of user is smoker ',function(){
            var controller = applicationsController();
        var result = controller.hasSmokers(group);
        expect(result).toBe(true);
    });
     it('should check if in group any of user is NOT smoker ',function(){
        var controller = applicationsController();
        var result = controller.hasSmokers(group);
        expect(result).toBe(false);
    });
     it('should check if class is enabled or disabled ',function(){
        var controller = applicationsController();
        var isEnabled = true;
        var result = controller.ngClass(dummyCSSClass,true);
        var result2 = controller.ngClass(dummyCSSClass,null);

        expect(result.isEnabled).toBe(isEnabled);
        expect(result.isEnabled).toBe(true);

     });
    it('should check if months is not number ' , function() {
        var controller = applicationsController();
        var result = controller.pluralMonths(twelve);
        expect(controller.pluralMonths(tweleve)).toEqual("");        
    });
    it('should check if months is not number ' , function() {
        var monthCount = 12;
        var controller = applicationsController();
        var result = controller.pluralMonths(month);
        var pluralMonth = monthCount + " " + (pluralize('month', monthCount));
        expect(controller.pluralMonths(monthCount)).toEqual(pluralMonth);
      });
     it('should toggle the group must return true',function() {
        var controller = applicationsController();
        var result = controller.toggleGroup(group);
        expect(result).toEqual(true);   
     });
    it('should return more if pass expanded true',function() {
        var controller = applicationsController();
        var result = controller.expandIconClass(group);
        expect(result).toEqual(true);   
     });
    it('should return less  if pass not expanded',function() {
        var controller = applicationsController();
        var result = controller.toggleGroup(group); //can pass here another object with expanded false
        expect(result).toEqual(false);   
     });
   
});